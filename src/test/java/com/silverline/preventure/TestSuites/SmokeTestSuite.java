package com.silverline.preventure.TestSuites;

/**
 * @author vmerkotan
 */

import org.junit.BeforeClass;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

import com.silverline.preventure.Tests.BaseTest;
import com.silverline.preventure.Tests.SmokeTests;


@RunWith(Suite.class)
@Suite.SuiteClasses({SmokeTests.class})
public class SmokeTestSuite {
	
	@BeforeClass
	public static void beforeSuite() {
		BaseTest bt = new BaseTest();
		bt.setOS("Windows");
		bt.setOSVersion("7");
		bt.setBrowserName("chrome");
	}	
}
