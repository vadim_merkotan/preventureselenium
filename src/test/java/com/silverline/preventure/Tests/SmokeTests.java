package com.silverline.preventure.Tests;

/**
 * @author vmerkotan
 */

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.silverline.preventure.PageBlocks.LoginPage;

public class SmokeTests extends BaseTest{
	
	@Before
	public void setUp() throws MalformedURLException {	
	
		driver = new RemoteWebDriver(
				new URL(
						"http://marina67:wFjnzAm76t5DryNYxrq3@hub.browserstack.com/wd/hub"),
				capability);
		
//		System.setProperty("webdriver.chrome.driver", "D://tools//chromedriver.exe");
//		driver = new ChromeDriver();
		
		driver.manage().window().maximize(); 
		
	}
	
	@After
	public void tearDown() {
		if (driver != null)
			driver.quit();
	}
	
	@Test
	public void testPVT680ValidateCommunityLoginPage() {
		
		System.out.println("[INFO] ******* test PVT680 Validate Community Login Page starts *******");
		System.out.println("Session ID:" + driver.getSessionId());
		LoginPage lp = new LoginPage(driver);
		lp.open();		
		Assert.assertTrue("Some errors present. Please reffer to log trace", lp.verifyDefaultState());
		System.out.println("[INFO] ******* test PVT680 Validate Community Login Page ends *******");
	}
	
	@Test
	public void testPVT681ValidateCommunityHomePage() {
		System.out.println("[INFO] ******* test PVT681 Validate Community Home Page starts *******");
		System.out.println("Session ID:" + driver.getSessionId());
		LoginPage lp = new LoginPage(driver);
		
		boolean result = lp.open()
		.login()
		.verifyDefaultState();
		
		Assert.assertTrue("Some errors present. Please reffer to log trace", result);
		
		System.out.println("[INFO] ******* test PVT681 Validate Community Home Page ends *******");
	}
	
}
