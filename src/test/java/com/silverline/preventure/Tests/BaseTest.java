package com.silverline.preventure.Tests;

/**
 * @author vmerkotan
 */

import org.junit.BeforeClass;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseTest {
	
	protected static String os = null;
	protected static String os_version = null;
	protected static String browser = null;
	protected static String url = null;
	
	protected static RemoteWebDriver driver;
	
	static DesiredCapabilities capability = new DesiredCapabilities();
	
	
	public void setOS (String os) {
		BaseTest.os = os;
	}
	
	public void setOSVersion (String osVersion) {
		os_version = osVersion;
	}
	
	public void setBrowserName (String browserName) {
		browser = browserName;
	}
	
	public void setURL (String url) {
		BaseTest.url = url;
	}	
	
	@BeforeClass
	public static void before () {
	capability.setCapability("os", os);
	capability.setCapability("os_version", os_version);
	capability.setCapability("browser", browser);				
	capability.setCapability("build", "JUnit - Sample");
	capability.setCapability("acceptSslCerts", "true");
	capability.setCapability("browserstack.debug", "true");
	capability.setCapability("resolution", "1920x1080");
	}
}
