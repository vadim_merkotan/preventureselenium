package com.silverline.preventure.PageBlocks;

/**
 * @author vmerkotan
 */

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage{

	public LoginPage (RemoteWebDriver driver) {
		super(driver);	
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text() = 'Maxwell Login']")
	WebElement header;

	@FindBy(xpath = "//input[@placeholder = 'Username']")
	WebElement userNameField;

	@FindBy(xpath = "//input[@placeholder = 'Password']")
	WebElement passwordField;

	@FindBy(xpath = "//button[text() = 'Login']")
	WebElement loginButton;

	@FindBy(linkText = "Forgot password?")
	WebElement forgotPasswordLink;

	@FindBy(xpath = "//span[text() = 'Need help? Call 888-321-4326']")
	WebElement helpText;



	/**
	 * open test org and wait till header become visible
	 * @return instance of Login Page<code>new LoginPage(driver)</code> 
	 * @author vmerkotan
	 */
	public LoginPage open() {
		driver.get("https://qa-preventure-qa.cs17.force.com/CustomCommunitiesLogin");		
		if (!waitElementVisible(header, 10, "[ERROR] header text: 'Maxwell Login' did not become visible in 10 seconds on Login Page.")) {
			throw new RuntimeException("[ERROR] header text: 'Maxwell Login' did not become visible in 10 seconds on Login Page.");
		}

		return new LoginPage(driver);
	}

	/**
	 * Verify presence of header text "Maxwell Login", user Name input field, password input field,
	 * login button, forgot password link, help text phone number
	 * @return true if all mentioned elements are present and visible, else - false
	 * @author vmerkotan
	 */
	public boolean verifyDefaultState() {
		boolean result = true;
		
		if (!waitElementVisible(header, 10, "[ERROR] header text: 'Maxwell Login' did not become visible in 10 seconds on Login Page."))
			result = false;
		
		if (!waitElementVisible(userNameField, 10, "[ERROR] UserName field with placeholder did not become visible in 10 seconds on Login Page."))
			result = false;
		
		if (!waitElementVisible(passwordField, 10, "[ERROR] Password field with placeholder did not become visible in 10 seconds on Login Page."))
			result = false;
		
		if (!waitElementVisible(loginButton, 10, "[ERROR] Login button did not become visible in 10 seconds on Login Page."))
			result = false;
		
		if (!waitElementVisible(forgotPasswordLink, 10, "[ERROR] Forgot password link did not become visible in 10 seconds on Login Page."))
			result = false;
		
		if (!waitElementVisible(helpText, 10, "[ERROR] Help text did not become visible in 10 seconds on Login Page."))
			result = false;

		return result;
	}
	
	/**
	 * logins with credentials js/maxwellforc4
	 * @return instance of Home Page <code>new HomePage(driver)</code>
	 * @author vmerkotan
	 */
	public HomePage login() {
		userNameField.clear();
		userNameField.sendKeys("js");
		passwordField.clear();
		passwordField.sendKeys("maxwellforc4");
		loginButton.click();
		return new HomePage(driver);		
	}
	
}
