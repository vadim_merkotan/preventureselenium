package com.silverline.preventure.PageBlocks;

/**
 * @author vmerkotan
 */
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {
	
	protected RemoteWebDriver driver;
	
	public AbstractPage(RemoteWebDriver driver) {
		this.driver = driver;
	}
	
	/**
	 * waits element to become visible
	 * @param element - WebElement
	 * @param timeout - seconds. Seconds to wait element to become clickable
	 * @return WebElement
	 * @author vmerkotan
	 */
	protected WebElement waitElementVisible (WebElement element, long timeout) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
		return element;
	}
	
	/**
	 * waits element to become clickable
	 * @param element - WebElement
	 * @param timeout - long. Seconds to wait element to become clickable
	 * @return WebElement
	 * @author vmerkotan
	 */
	protected WebElement waitElementClickable (WebElement element, long timeout) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
		return element;
	}
	
	/**
	 * waits element to become visible, catches TimeoutException print error message to console
	 * @param element - WebElement
	 * @param timeout - long. Seconds to wait element to become visible
	 * @param errorMessage - String. Error message to print to console if TimeoutException
	 * appears
	 * @return true if element become visible after during timeout, else - false 
	 * @author vmerkotan
	 */
	public boolean waitElementVisible(WebElement element, long timeout, String errorMessage) {
		try {
			waitElementVisible(element, timeout);
		} catch (TimeoutException t) {
			System.out.println(errorMessage);
			return false;
		}
		return true;
	}
	
	/**
	 * waits element to become clickable, catches TimeoutException print error message to console
	 * and throw this exception
	 * @param element - WebElement
	 * @param timeout - long. Seconds to wait element to become clickable
	 * @param errorMessage - String. Error message to print to console if TimeoutException
	 * appears
	 * @return WebElement 
	 * @author vmerkotan
	 */
	public WebElement waitElementClickable(WebElement element, long timeout, String errorMessage) {
		try {
			return waitElementClickable(element, timeout);
		} catch (TimeoutException t) {
			System.out.println(errorMessage);
			throw t;
		}
	}
	
	
	
	
}
