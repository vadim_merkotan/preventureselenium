package com.silverline.preventure.PageBlocks;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** 
 * @author vmerkotan
 */

public class HomePage extends AbstractPage{

	@FindBy (xpath = "//*[text() = 'Dashboard']")
	WebElement dashboardText;

	@FindBy (xpath = "//span[text() = 'Earn']")
	WebElement earnButton;

	@FindBy (linkText = "Events")
	WebElement eventsLink;

	@FindBy (linkText = "Track")
	WebElement trackLink;

	@FindBy (linkText = "Social")
	WebElement socialLink;

	@FindBy (className = "glyphicon-inbox")
	WebElement messagesButton;

	@FindBy (className = "glyphicon-user")
	WebElement profileButton;

	@FindBy (linkText = "Campaigns")
	WebElement campaignsLink;

	@FindBy (linkText = "Question Sets")
	WebElement questionSetsLink;

	@FindBy (linkText = "no new messages")
	WebElement noNewMessagesLink;

	@FindBy (linkText = "Settings")
	WebElement SettingsLink;

	@FindBy (linkText = "Achievements")
	WebElement achievementsLink;

	@FindBy (linkText = "Devices")
	WebElement devicesLink;

	@FindBy (linkText = "Logout")
	WebElement logoutLink;

	public HomePage (RemoteWebDriver driver) {
		super(driver);	
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * verifies that "Dashboard Text", "Events" button, "Social" link, "Track" link,
	 * "Earn" button with "Campaigns" and "Question Sets", Messages button with
	 * "no new message" text, "Profile" button with "Settings", "Achievements"
	 * "Devices", "Logout" links, are present on home page
	 * @return true if all mentioned elements visible, else - false
	 * @author vmerkotan
	 */
	public boolean verifyDefaultState() {
		boolean result = true;
		
		if (!waitElementVisible(dashboardText, 10, "[ERROR] dashboard text: 'Dashboard text' did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(eventsLink, 10, "[ERROR] 'Events' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(socialLink, 10, "[ERROR] 'Social' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(trackLink, 10, "[ERROR] 'Track' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if(!verifyEarnButtonDropdownContent() || 
				!verifyMessagesButtonDropdownContent() ||
				!verifyProfileButtonDropdownContent()) {
			result = false;
		}
		
		return result;
	}

	/**
	 * verifies that "Earn" button with "Campaigns" and "Question Sets" are present and visible
	 * @return true if all mentioned elements are present and visible, else - false
	 * @author vmerkotan
	 */
	private boolean verifyEarnButtonDropdownContent() {
		boolean result = true;
		
		waitElementClickable(earnButton, 10, "[ERROR] 'Earn' button did not become clickable in 10 seconds on Home Page.")
		.click();
		
		if (!waitElementVisible(campaignsLink, 10, "[ERROR] 'Campaings' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(questionSetsLink, 10, "[ERROR] 'Question Sets' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		return result;
	}
	
	/**
	 * verifies that "Messages" button with "no new message" text is present and visible
	 * @return true if all mentioned elements are present and visible, else - false
	 * @author vmerkotan
	 */
	private boolean verifyMessagesButtonDropdownContent() {
		
		waitElementClickable(messagesButton, 10, "[ERROR] 'Messages' button did not become clickable in 10 seconds on Home Page.")
		.click();
		
		if (!waitElementVisible(noNewMessagesLink, 10, "[ERROR] 'No new message' text did not become visible in 10 seconds on Home Page.")) {
			return false;		
		} else {
			return true;
		}
	}
	
	/**
	 * verifies that "Profile" button with "Settings", "Achievements", "Devices", "Logout" links
	 * is present and visible
	 * @return true if all mentioned elements are present and visible, else - false
	 * @author vmerkotan
	 */
	private boolean verifyProfileButtonDropdownContent() {
		boolean result = true;
		
		waitElementClickable(profileButton, 10, "[ERROR] 'Profile' button did not become clickable in 10 seconds on Home Page.")
		.click();
		
		if (!waitElementVisible(SettingsLink, 10, "[ERROR] 'Settings' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(achievementsLink, 10, "[ERROR] 'Achievements' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(devicesLink, 10, "[ERROR] 'Devices' link did not become visible in 10 seconds on Home Page."))
			result = false;
		
		if (!waitElementVisible(logoutLink, 10, "[ERROR] 'Logout' link did not become visible in 10 seconds on Home Page."))
			result = false;

		return result;
	}


}
